import tensorflow as tf
import string
import re
from tensorflow import keras

def custom_standardization(input_data):
  lowercase = tf.strings.lower(input_data)
  stripped_html = tf.strings.regex_replace(lowercase, '<br />', ' ')
  return tf.strings.regex_replace(stripped_html,
                                  '[%s]' % re.escape(string.punctuation),
                                  '')

model = keras.models.load_model('model' , custom_objects={'custom_standardization': custom_standardization})

examples = [
  "the best tweet of my life!!",
  "yeah pretty okay tweet",
  "this is the WORST tweet ever"
]

print(model.predict(examples))